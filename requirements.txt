Django>=4.0.0,<5.0.0
djangorestframework>=3.0.0,<3.20.0
psycopg2>=2.9.9,<3.0.0
Pillow>=5.3.0,<5.4.0
uwsgi>=2.0.18,<2.1.0
boto3>=1.34.9,<1.35.0
django-storages>=1.14.2,<1.15.0
gunicorn
flake8>=7.0.0,<8.0.0
