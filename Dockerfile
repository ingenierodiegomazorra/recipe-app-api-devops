FROM python:3.10-alpine
LABEL maintainer="Dimansoft Developer Ltd"

ENV PYTHONUNBUFFERED 1
ENV PATH="/scripts:${PATH}"

RUN pip install --upgrade pip

COPY ./requirements.txt /requirements.txt
RUN apk add --update --no-cache postgresql-client jpeg-dev
RUN apk add --update --no-cache --virtual .tmp-build-deps \
      gcc libc-dev linux-headers gcc python3-dev musl-dev postgresql-dev zlib zlib-dev libpq-dev
RUN pip install -r /requirements.txt



# Check if python3-dev is properly installed, especially the distutils
RUN python3 -c "import distutils.util" || apk fix python3-dev



RUN apk del .tmp-build-deps

RUN mkdir /app
WORKDIR /app
COPY ./app /app
COPY ./scripts /scripts
RUN chmod +x /scripts/*

RUN mkdir -p /vol/web/media
RUN mkdir -p /vol/web/static
RUN adduser -D user
RUN chown -R user:user /vol/
RUN chmod -R 755 /vol/web
USER user
VOLUME /vol/web

EXPOSE 8000

CMD ["entrypoint.sh"]
